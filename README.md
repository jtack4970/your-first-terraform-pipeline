# Intro to Terraform Presentation

**Author: James Tacker**

## Prerequisites

1. [git Installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. [GitLab Account](https://gitlab.com/users/sign_up)
3. [NodeJS v18](https://nodejs.org/en/download) or higher
4. [AWS Access keys](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html) (optional if you want to run through the labs)

## Getting started

1. Clone this repo using `git`
    ```
    git clone https://gitlab.com/jtack4970/your-first-terraform-pipeline.git
    ```
2. While in the root project directory, open the `presentation/` directory
   ```shell
   cd presentation
   ```

## Local Deployment

1. Install the dependencies using `npm`
   ```shell
   npm install
   ```
2. Run the HTTP server to view the presentation
   ```shell
   npm start
   ```
   
3. Use the arrow keys to change slides


## Gitpod Deployment
If you don't want to set up your own IDE or local environment click the gitpod button below to get started:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/jtack4970/your-first-terraform-pipeline/-/tree/main/)

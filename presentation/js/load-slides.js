// js/load-slides.js

// Function to load slides sequentially
function loadSlidesSequentially() {
    const slidesContainer = document.getElementById('slides-container');
    const slidePaths = [
        './slides/slide1.html',
        './slides/slide2.html',
        './slides/slide3.html',
        './slides/slide4.html',
        './slides/slide5.html',
        './slides/labs/lab1.html',
        './slides/slide6.html',
        './slides/slide7.html',
        './slides/slide8.html',
        './slides/slide9.html',
        './slides/slide10.html',
        './slides/slide11.html',
        './slides/slide12.html',
        './slides/labs/lab2-1.html',
        './slides/labs/lab2-2.html',
        './slides/labs/lab2-3.html',
        './slides/slide13.html',
        './slides/slide14.html',
        './slides/slide15.html',
        // './slides/slide16.html',
        './slides/labs/lab3-1.html',
        './slides/labs/lab3-2.html',
        './slides/labs/lab3-3.html',
        './slides/slide17.html',
        './slides/labs/bonus-lab1.html',
        './slides/slide18.html'
    ]; // Add paths to your slide HTML files

    // Helper function to load a single slide
    function loadSlide(index) {
        if (index >= slidePaths.length) {
            // All slides have been loaded
            return;
        }

        fetch(slidePaths[index])
            .then((response) => response.text())
            .then((html) => {
                const slide = document.createElement('section');
                slide.innerHTML = html;
                slidesContainer.appendChild(slide);

                // Load the next slide
                loadSlide(index + 1);
            });
    }

    // Start loading the slides from the beginning
    loadSlide(0);
}

// Call the loadSlidesSequentially function when Reveal.js is ready
Reveal.addEventListener('ready', loadSlidesSequentially);

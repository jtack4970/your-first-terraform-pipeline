terraform {
  backend "s3" {
    bucket = "your-first-terraform-pipelines-tfstate"
    key    = "./terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "your-first-terraform-pipelines-tfstate-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "web" {
  ami           = "ami-08a52ddb321b32a8c"
  instance_type = "t3.micro"
}